package fr.univtln.m2dapm.mbernard232.codereader;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.FocusingProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by maxime on 06/01/18.
 */

public class BarcodeProcessor extends FocusingProcessor<Barcode> {

    public BarcodeProcessor(Detector detector, Tracker tracker) {
        super(detector, tracker);
    }

    @Override
    public int selectFocus(Detector.Detections detections) {
        // We only care about the first code found, the app is not here to read several codes at the same time //
        return detections.getDetectedItems().keyAt(0);
    }
}
