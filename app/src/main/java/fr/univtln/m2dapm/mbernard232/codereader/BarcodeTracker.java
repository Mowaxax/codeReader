package fr.univtln.m2dapm.mbernard232.codereader;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by maxime on 06/01/18.
 */

public class BarcodeTracker extends Tracker<Barcode> {

    private final CodeReaderActivity codeReaderActivity;

    public BarcodeTracker(CodeReaderActivity codeReaderActivity) {
        this.codeReaderActivity = codeReaderActivity;
    }

    @Override
    public void onNewItem(int i, Barcode barcode) {
        super.onNewItem(i, barcode);
        codeReaderActivity.onBarcode(barcode);
    }
}
