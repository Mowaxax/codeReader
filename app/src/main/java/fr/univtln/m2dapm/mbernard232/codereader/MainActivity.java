package fr.univtln.m2dapm.mbernard232.codereader;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.vision.barcode.Barcode;

public class MainActivity extends AppCompatActivity {

    public static final int READ_CODE = 0;

    // Graphical elements of the view //
    private TextView tv_code;
    private TextView tv_debug;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Retrieves the graphical elements //
        tv_code = (TextView) findViewById(R.id.tv_code);
        tv_debug = (TextView) findViewById(R.id.tv_debug);

        // Checks permissions //
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling ActivityCompat#requestPermissions here to request the missing CAMERA permission, and then overriding
            // public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case READ_CODE:
                if(resultCode == RESULT_OK) {
                    String code = data.getStringExtra(CodeReaderActivity.CODE_READ);
                    setCode(formatBarcodeValue(code));
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private String formatBarcodeValue(String barcode) {
        return barcode.replaceAll("^([0-9])([0-9]{6})([0-9]{6})$", "$1 $2 $3");
    }

    public void startRead(View view) {
        Intent readCodeIntent = new Intent(this, CodeReaderActivity.class);
        startActivityForResult(readCodeIntent, READ_CODE);
    }

    public void setCode(final String code) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_code.setText(code);
            }
        });
    }

    public void setDebug(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_debug.setText(text);
            }
        });
    }
}
