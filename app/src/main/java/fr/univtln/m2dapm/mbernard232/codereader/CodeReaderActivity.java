package fr.univtln.m2dapm.mbernard232.codereader;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

public class CodeReaderActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    public static final String CODE_READ = "fr.univtln.m2dapm.mbernard232.codereader.CodeReaderActivity.CODE_READ";

    // Graphical elements of the view //
    private SurfaceView sv_camera;

    // Classes used to read the barcode //
    private CameraSource cameraSource;
    private BarcodeDetector barcodeDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code_reader);

        // Retrieves the graphical elements //
        sv_camera = (SurfaceView) findViewById(R.id.sv_camera);

        // Initializes the BarcodeDetector //
        barcodeDetector = new BarcodeDetector.Builder(getApplicationContext()).setBarcodeFormats(Barcode.EAN_13).build();
        barcodeDetector.setProcessor(new BarcodeProcessor(barcodeDetector, new BarcodeTracker(this)));

        // Initializes the CameraSource //
        cameraSource = new CameraSource.Builder(getApplicationContext(), barcodeDetector).setAutoFocusEnabled(true)
                .setFacing(CameraSource.CAMERA_FACING_BACK).setRequestedFps(15).build();

        // Uses callback to use the SurfaceView AFTER its creation to display the camera
        sv_camera.getHolder().addCallback(this);
    }

    @Override
    protected void onStop() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            cameraSource.release();
        }
        super.onStop();
    }

    public void onBarcode(Barcode barcode) {
        Intent data = new Intent();
        data.putExtra(CODE_READ, barcode.displayValue);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            try {
                cameraSource.start(sv_camera.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // Nothing to do
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // Nothing to do
    }
}
